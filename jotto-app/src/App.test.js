import React from 'react';
import { shallow } from 'enzyme';
import { findByTestAttr } from '../test/testUtils';

import App from './App';
import { exportAllDeclaration } from '@babel/types';

const setup = () => {
  return shallow(<App />);
}

it('App renders without crashing', () => {
  const wrapper = setup();
  const component = findByTestAttr(wrapper, 'component-app');
  expect(component.length).toBe(1);
});

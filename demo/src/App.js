import React, { Component } from 'react';

import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
      error: false
    };
  }
  incrementCounter = () => {
    let currentCount = this.state.counter;
    let nextState;
    currentCount = currentCount + 1;
    nextState = {
      counter: currentCount,
      error: false
    }
    this.setState(nextState);
  }
  decrementCounter = () => {
    let currentCount = this.state.counter;
    let nextState;
    currentCount = currentCount - 1;
    nextState = {
      counter: currentCount
    }
    if (currentCount < 0) {
      nextState = {
        error: true
      }
    }
    this.setState(nextState);
  }
  render() {
    return (
      <div data-test="component-app">
        <hgroup>
          <h1 data-test="counter-display">The counter is currently {this.state.counter}</h1>
          {this.state.error ? (<h2
            data-test="error-display"
            style={{ color: 'red' }}>The counter can't go below zero.</h2>) : null}
        </hgroup>
        <button
          data-test="increment-button"
          onClick={this.incrementCounter}
        >Increment counter</button>
        <button
          data-test="decrement-button"
          onClick={this.decrementCounter}
        >Decrement counter</button>
      </div >
    );
  }

}

export default App;

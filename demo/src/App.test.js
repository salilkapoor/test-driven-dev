import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import EnzymeAdapter from 'enzyme-adapter-react-16';

import App from './App';

Enzyme.configure({ adapter: new EnzymeAdapter() });

const setup = (props = {}, state = null) => {
  const wrapper = shallow(<App {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
}

const findByTestAttr = (wrapper, val) => {
  return wrapper.find(`[data-test="${val}"]`);
}

it('renders without error', () => {
  const wrapper = setup();
  const appComponent = findByTestAttr(wrapper, 'component-app');
  expect(appComponent.length).toBe(1);
});

it('renders increment button', () => {
  const wrapper = setup();
  const btnComponent = findByTestAttr(wrapper, 'increment-button');
  expect(btnComponent.length).toBe(1);
});

it('renders decrement button', () => {
  const wrapper = setup();
  const btnComponent = findByTestAttr(wrapper, 'decrement-button');
  expect(btnComponent.length).toBe(1);
});

it('renders counter display', () => {
  const wrapper = setup();
  const headerComponent = findByTestAttr(wrapper, 'counter-display');
  expect(headerComponent.length).toBe(1);
});

it('renders error display', () => {
  const error = true;
  const wrapper = setup(null, { error });
  const errorDisplay = findByTestAttr(wrapper, 'error-display');
  expect(errorDisplay.length).toBe(1);
});

it('counter starts at 0', () => {
  const wrapper = setup();
  const initialCounterState = wrapper.state('counter');
  expect(initialCounterState).toBe(0);
});

it('clicking button increments counter display', () => {
  const counter = 7;
  const wrapper = setup(null, { counter });
  const btnComponent = findByTestAttr(wrapper, 'increment-button');
  btnComponent.simulate('click');

  const headerComponent = findByTestAttr(wrapper, 'counter-display');
  expect(headerComponent.text()).toContain(counter + 1);
});

it('clicking button decrements counter display', () => {
  const counter = 7;
  const wrapper = setup(null, { counter });
  const btnComponent = findByTestAttr(wrapper, 'decrement-button');
  btnComponent.simulate('click');

  const headerComponent = findByTestAttr(wrapper, 'counter-display');
  expect(headerComponent.text()).toContain(counter - 1);
});




